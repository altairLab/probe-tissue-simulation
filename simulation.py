import Sofa.Simulation, Sofa.Gui, Sofa.Core, SofaRuntime
import os, time
import numpy as np

from components.utils import Parameters, Analysis, matrix2xyzquat
from components.header import add_scene_header
from objects.breast import Breast, Lesion
from objects.probe import Probe

class BreastProbe(Sofa.Core.Controller):
    """ Sofa Controller representing an US probe that comes in contact with a breast with an embedded lesion"""
    def __init__(
        self,
        root: Sofa.Core.Node,
        params: Parameters
        ):
        """ 
        Args:
            root (Sofa.Core.Node): root node of the simulation.
            params (Parameters): class containing all the parameters provided in the yml configuration file.
        """
        Sofa.Core.Controller.__init__(self)
        self.root = root

        # Output files
        outdir = os.path.join( params.outdata_dir, f"tumor{params.tumorID}" )
        os.makedirs(outdir, exist_ok=True)
        self.lesion_output_filename = os.path.join(outdir, f"Fiducial")
        self.time_output_filename   = os.path.join(outdir, f"Time")

        # Create header
        add_scene_header( root, 
                            gravity=params.gravity,
                            dt=params.dt,
                            alarm_distance=params.alarm_distance,
                            contact_distance=params.contact_distance
                        )

        # Initial transform for the breast
        breast_transform = np.identity(4)
        # Read fixed indices from file
        fixed_indices = np.loadtxt(params.breast_fixed_file, dtype=int).tolist()
        # Create breast
        self.breast = root.addObject(    
                                Breast( root, 
                                        volume_filename=params.breast_volume_file,
                                        collision_filename=params.breast_collision_file,
                                        init_tf=breast_transform,
                                        density=params.rho,
                                        material=params.material,
                                        E=params.E,
                                        nu=params.nu,
                                        fixed_indices=fixed_indices,
                                        visual_filename=params.breast_visual_file
                                      )
                                )

        # Read lesion position
        lesion_filename = f"{params.lesion_basedir}{params.tumorID}/Fiducial0.txt"
        lesion_position = np.loadtxt(lesion_filename)
        # Create lesion
        self.lesion = root.addObject(
                                Lesion( self.breast.node, 
                                        lesion_position=lesion_position,
                                        init_tf=breast_transform,
                                        node_name=f"Lesion{params.tumorID}")
                                    )

        # Create probe motion
        target_positions = []
        for i in range(1, params.num_deformations+1):
            probe_tf_filename = f"{params.lesion_basedir}{params.tumorID}/Transform{i}.txt"
            probe_matrix = np.loadtxt(probe_tf_filename)
            target_positions.append( [probe_matrix[0,3], probe_matrix[1,3], probe_matrix[2,3]] )
        target_positions = np.asarray(target_positions)
        target_positions = target_positions.reshape((-1,3))

        # Read probe initial transform
        probe_tf_filename = f"{params.lesion_basedir}{params.tumorID}/Transform0.txt"
        probe_matrix = np.loadtxt(probe_tf_filename)
        probe_pose   = matrix2xyzquat(probe_matrix, offset=np.asarray(params.probe_initial_offset))
        # Create probe
        self.probe = root.addObject(
                                Probe( root, 
                                        collision_filename=params.probe_collision_file,
                                        visual_filename=params.probe_visual_file,
                                        init_pose=probe_pose,
                                        target_position=target_positions,
                                        velocity=params.probe_velocity
                                        )
                                )
        
        # Outputs
        self.lesion_positions = []
        self.time_per_def = []
        self.start_time = time.time()

    def onAnimateBeginEvent(self, __):
        # if current deformation has ended
        if self.probe.current_def_ended:
            # Save lesion position
            lesion_position = np.mean(self.lesion.state.position.value, axis=0)
            self.lesion_positions.append(lesion_position.tolist())
            # Save time statistics
            self.time_per_def.append(time.time()-self.start_time)
            self.start_time = time.time()

        if (self.probe.current_def_ended and not(len(self.probe.motion_path))) or (not(self.breast.is_stable)):
            self.root.animate = False
            # Saving lesion position and time
            self.lesion_positions = np.asarray(self.lesion_positions)
            np.savez_compressed(self.lesion_output_filename, self.lesion_positions)
            print(f"Lesion positions saved in {self.lesion_output_filename}")
            np.savez_compressed(self.time_output_filename, self.time_per_def)
            print(f"Time per deformations saved in {self.time_output_filename}")

def createScene(root, params):
    root.addObject( BreastProbe(root, params) )
    return root
    

if __name__ == '__main__':
    
    # Make sure to load all SOFA libraries
    plugins = ["SofaComponentAll"]
    for p in plugins:
        SofaRuntime.importPlugin(p)

    #Create the root node
    root = Sofa.Core.Node("root")

    # Load parameters
    params = Parameters("./input_parameters.yml")

    # Check if the needed analysis type is implemented
    assert Analysis(params.type) in Analysis, f"Invalid choice for simulation approach {params.type}"
    print(f"\n\n\nCreating simulation with {params.type} approach")

    # Call 'createScene' function to create the scene graph
    createScene(root, params)
    Sofa.Simulation.init(root)

    if not params.use_gui:
        # Execute simulation in background, without GUI
        for iteration in range(100000):
            Sofa.Simulation.animate(root, root.dt.value)
    else:
        # Find out the supported GUIs
        print ("Supported GUIs are: " + Sofa.Gui.GUIManager.ListSupportedGUI(","))
        # Launch the GUI (qt or qglviewer)
        Sofa.Gui.GUIManager.Init("myscene", "qglviewer")
        Sofa.Gui.GUIManager.createGUI(root, __file__)
        Sofa.Gui.GUIManager.SetDimension(1080, 1080)
        # Initialization of the scene will be done here
        Sofa.Gui.GUIManager.MainLoop(root)
        Sofa.Gui.GUIManager.closeGUI()
        print("GUI was closed")

    print("Simulation is done.")