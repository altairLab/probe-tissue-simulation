import Sofa.Core

def add_scene_header(
        root: Sofa.Core.Node,
        gravity: list = [0, -9.81, 0],
        dt: float = 0.01,
        alarm_distance: float = 0.002,
        contact_distance: float = 0.001,
        ):
    root.dt.value = dt
    root.gravity.value = gravity
    root.addObject('DefaultVisualManagerLoop')
    root.addObject('VisualStyle', displayFlags='hideCollisionModels hideForceFields hideBehaviorModels showVisualModels')
    root.addObject('BackgroundSetting', color=[0, 0, 0, 0])

    root.addObject('FreeMotionAnimationLoop')
    root.addObject('GenericConstraintSolver', 
                    maxIterations=1000, 
                    tolerance=1e-6, 
                    printLog=0, 
                    allVerified=0
                    )

    # root.addObject('LCPConstraintSolver', 
    #                 maxIterations=1000, 
    #                 tolerance=1e-6, 
    #                 printLog=0, 
    #                 allVerified=0
    #                 )
    root.addObject('DefaultPipeline')
    root.addObject('BruteForceBroadPhase')
    root.addObject('BVHNarrowPhase')
    root.addObject('LocalMinDistance',
                    alarmDistance = alarm_distance, 
                    contactDistance = contact_distance, 
                    angleCone = 90.0, 
                    filterIntersection = 0
                    )
    root.addObject('DefaultContactManager', 
                    name = "ContactResponse", 
                    response = "FrictionContactConstraint", 
                    responseParams = "mu=0"
                    )
